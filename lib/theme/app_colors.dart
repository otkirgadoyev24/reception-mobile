import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../controller/settings_controller.dart';

final settingController = Get.put(SettingControllerCustom());



class AppColor extends GetxController {
  Color cardColor = settingController.isDarkMode.value
      ?  Color(0xff272E38)
      :  Colors.white;
  Color black = settingController.isDarkMode.value
      ? const Color(0xFFE9EDF3)
      : const Color(0xFF273244);
  Color bgColor = settingController.isDarkMode.value
      ? const Color(0xff181D24)
      : const Color(0xFFF8FAFE);
  Color iconColor = settingController.isDarkMode.value== false
      ? const Color(0xff181D24)
      : const Color(0xFFF8FAFE);
  Color kgreyColor = const Color(0xff9CA4AB);
  Color appBgColor = const Color(0xffF8FAFE);
  Color kprimaryColor = Colors.blue;
  Color korangeColor = const Color(0xffFE970F);
  Color white = const Color(0xFFFFFFFF);
  Color lightGreyText = const Color(0xFF8E97A7);
  Color myBlack =  Color(0xFF273244);

  Color lightGrey = const Color(0XFFE9E9E9);
  Color lightBlue = const Color(0xFFEBF5FC);
  Color borderColor = settingController.isDarkMode.value
      ? const Color(0xFF3D444F)
      : const Color(0xFFCDD4E0);
}

class NightMode {
  static const Color cardColor = Color(0xff9CA4AB);
}

class DayMode {
  static const Color cardColor = Color(0xff9CA4AB);
}










const Color kgreyColor = Color(0xff9CA4AB);
const Color appBgColor = Color(0xffF8FAFE);
const Color borderColor = Color(0xffCDD4E0);
const Color foucusedborderColor = Color(0xff8F9CB1);
const Color kprimaryColor = Colors.blue;
const Color korangeColor = Color(0xffFE970F);
const Color white = Color(0xFFFFFFFF);
const Color black = Color(0xFF283344);
const Color lightGreyText = Color(0xFF8E97A7);
const Color lightGrey = Color(0XFFE9E9E9);
const Color greyBlue = Color(0xFFCDD4E0);
const Color midGrey = Color(0xffb1b7c2);
const Color icongreycolor = Color(0xffB2C5DC);


class MyColor {
  static const jobBackground = Color(0xFFEBF5FC);
  static const job = Color(0xFF3498DB);
  static const typeBk = Color(0xFFFFF5E7);
  static const type = Color(0xFFEC8C0E);
  static const experienceBackground = Color(0xFFE5F6F3);
  static const experience = Color(0xFF00A98A);
  static const salary = Color(0xFFE96B50);
  static const salartBackground = Color(0xFFFFF2EF);
  static const blackText = Color(0XFF171725);
}