import 'package:flutter/material.dart';

import 'app_colors.dart';

class CustomStyles{

  static const headerTextStyle =  TextStyle(
    fontSize: 34,
    fontWeight: FontWeight.w700,
    fontFamily: 'sfpro',
    height: 1.29,
    color: black,
  );

  static const subHeaderTextStyle =  TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color: lightGreyText,
  );

  static const widgetHeaderTextStyle =  TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w700,
    fontFamily: 'sfpro',
    color: black,
    height: 1.65
  );

  static const widgetSubHeaderTextStyle =  TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color: lightGreyText,
  );
  static const widgetSubHeaderTextStyle2 =  TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color: Color(0xFF273244),
    height: 1.65
  );

  static const searchWidgetTitle =  TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color:     black,
  );
  static const appBarTitleStyle =  TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w700,
    fontFamily: 'sfpro',
    color: black,
  );
  static const appBarSubtitleStyle =  TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color: lightGreyText,
  );
  static const topTitle =  TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color: lightGreyText,
  );
  static const workTimeTextStyle =  TextStyle(
    color: Color(0xFF273244),
    fontSize: 34,
    fontFamily: 'sfpro',
    fontWeight: FontWeight.w700,
  );


}
class CustomStyles1 {
  TextStyle headerTextStyle = TextStyle(
    fontSize: 34,
    fontWeight: FontWeight.w700,
    fontFamily: 'sfpro',
    color: AppColor().black,
  );


    TextStyle topTitle =  TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color: AppColor().lightGreyText,
  );

   TextStyle widgetSubHeaderTextStyle =  TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    height: 1.65,
      color: AppColor().iconColor,


  );
  TextStyle subHeaderTextStyle = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    height: 1.54,
    color: AppColor().lightGreyText,
  );

  TextStyle widgetHeaderTextStyle = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w700,
    fontFamily: 'sfpro',
    color: AppColor().black,
  );



  TextStyle searchWidgetTitle = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color: AppColor().black,
  );
  TextStyle appBarTitleStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w700,
    fontFamily: 'sfpro',
    color: AppColor().black,
  );
  TextStyle appBarSubtitleStyle = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color: AppColor().lightGreyText,
  );

  TextStyle catalogTitle = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w400,
    fontFamily: 'sfpro',
    color: AppColor().lightGreyText,
  );
}