import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:statistic_app_v1/auth/login.dart';
import 'package:statistic_app_v1/controller/login.dart';

import 'localization/translation_words.dart';

void main() async {
  await GetStorage.init();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  WidgetsFlutterBinding.ensureInitialized();
  runApp( MyApp());
}

class MyApp extends StatelessWidget {

  final LoginController loginController = Get.put(LoginController());
  final box = GetStorage();

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      translations: Languages(),
      locale: Locale( box.read("lang_code_1") ?? 'ru', box.read('lang_code_2') ?? 'RU'),
      fallbackLocale: Locale(
          box.read("lang_code_1") ?? 'ru', box.read('lang_code_2') ?? 'RU'),
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: Login(),
    );
  }
}