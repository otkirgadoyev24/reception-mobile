import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:statistic_app_v1/pages/Settings.dart';
import 'package:statistic_app_v1/theme/app_colors.dart';

import '../pages/MainScreen.dart';
import '../pages/murojaatlar_diagrammasi.dart';



class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static RxInt currentScreen = 0.obs;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Statistics(),
      // bottomNavigationBar: PhysicalModel(
      //   color: kprimaryColor,
      //   borderRadius: const BorderRadius.only(
      //     topRight: Radius.circular(35),
      //     topLeft: Radius.circular(35),
      //   ),
      //   shadowColor: Colors.black,
      //   elevation: 5.0,
      //   child: Container(
      //     padding: const EdgeInsets.symmetric(
      //       horizontal: 26,
      //     ),
      //     height: 100,
      //     width: Get.width,
      //     decoration: const BoxDecoration(
      //       borderRadius: BorderRadius.only(
      //         topRight: Radius.circular(35),
      //         topLeft: Radius.circular(35),
      //       ),
      //       color: kprimaryColor,
      //     ),
      //     child: Row(
      //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //       crossAxisAlignment: CrossAxisAlignment.center,
      //       children: [
      //         IconButton(
      //
      //           onPressed: () {
      //             currentScreen.value = 0;
      //           },
      //           icon: SizedBox(
      //               height: 28,
      //               width: 28,
      //               child:currentScreen.value == 0
      //                   ?  Image.asset('assets/images/home_choosen.png')
      //                   : Image.asset('assets/images/home/Home.png')),
      //         ),
      //         IconButton(
      //           onPressed: () {
      //             currentScreen.value = 1;
      //           },
      //           icon: SizedBox(
      //             height: 28,
      //             width: 28,
      //             child:  currentScreen.value == 1
      //                 ? Icon(Icons.insert_chart,color: Colors.white,size: 30,)
      //                 : Icon(Icons.insert_chart_outlined_sharp,color: Colors.white,size: 30,),
      //           ),
      //         ),
      //
      //         IconButton(
      //           onPressed: () {
      //             currentScreen.value = 2;
      //           },
      //           icon: SizedBox(
      //             height: 28,
      //             width: 28,
      //             child: currentScreen.value == 2
      //                 ? Image.asset('assets/images/cabinet_choosen.png')
      //                 : Image.asset('assets/images/home/Cabinet.png'),
      //           ),
      //         ),
      //       ],
      //     ),
      //   ),
      // ),
    );
  }
}