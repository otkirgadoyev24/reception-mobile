import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ResendMessageSnack {
  GetSnackBar customSnack({
    required String errorMessage,
  }) {
    return GetSnackBar(
      icon: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12,horizontal: 16),
        child:Icon(CupertinoIcons.checkmark_alt_circle,size: 40,color: Colors.white,),
      ),
      margin: const EdgeInsets.all( 16),
      padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 12),
      snackPosition: SnackPosition.TOP,
      duration: const Duration(milliseconds: 1200),
      borderRadius: 50,
      backgroundColor: const Color(0xff3DB6A0),
      messageText: Padding(
        padding: const EdgeInsets.only(left: 16.0),
        child: Text(errorMessage ,style:TextStyle(
          color: Colors.white,
          fontSize: 17,
          fontFamily: 'sfpro',
          fontWeight: FontWeight.w400,
          height: 1.65,
        ),),
      ),
    );
  }
}