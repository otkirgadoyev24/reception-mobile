import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SuccessMessageSnack {
  GetSnackBar customSnack({
    required String title,
    required String errorMessage,
  }) {
    return GetSnackBar(
      snackPosition: SnackPosition.TOP,
      duration: const Duration(milliseconds: 1200),
      borderRadius: 12,
      barBlur: 1.0,
      backgroundColor: Colors.white.withOpacity(0.8),
      titleText: Text(
        title,
        style: const TextStyle(
          color: Colors.red,
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ),
      messageText: Text(errorMessage),
    );
  }

  GetSnackBar editSnackBar({
    required String message,
  }) {
    return GetSnackBar(
      margin: const EdgeInsets.all(16),
      padding: const EdgeInsets.all(5),
      snackPosition: SnackPosition.TOP,
      duration: const Duration(milliseconds: 1200),
      borderRadius: 35,
      barBlur: 1.0,
      backgroundColor: const Color(0xFF3DB6A0),
      icon:const Padding(
        padding:  EdgeInsets.symmetric(vertical: 10.0, horizontal: 16),
        child:  Icon(
          Icons.check_circle_outline_outlined,
          size: 40,
          color: Colors.white,
        ),
      ),
      messageText: Padding(
        padding: const EdgeInsets.only(left: 16.0),
        child: Text(
          message,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    );
  }


  GetSnackBar cancelSnackBar({
    required String message,
  }) {
    return GetSnackBar(
      margin: const EdgeInsets.all(16),
     // padding: const EdgeInsets.symmetric(vertical: 12,horizontal: 16),
      snackPosition: SnackPosition.TOP,
      duration: const Duration(milliseconds: 1200),
      borderRadius: 35,
      barBlur: 1.0,
      backgroundColor: Colors.black,
      icon: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12,horizontal: 16),
        child: const Icon(
          Icons.cancel_outlined,
          size: 40,
          color: Colors.white,
        ),
      ),
      messageText: Padding(
        padding: const EdgeInsets.only(left: 16.0),
        child: Text(
          message,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    );
  }



}