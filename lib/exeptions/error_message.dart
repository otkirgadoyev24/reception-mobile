import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ErrorMessageSnack {
  GetSnackBar customSnack({
    required String title,
    required String errorMessage,
  }) {
    return GetSnackBar(
      snackPosition: SnackPosition.TOP,
      duration: const Duration(milliseconds: 1200),
      borderRadius: 12,
      barBlur: 1.0,
      backgroundColor: Colors.white.withOpacity(0.8),
      titleText: Text(
        title,
        style: const TextStyle(
          color: Colors.red,
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ),
      messageText: Text(errorMessage),
    );
  }

  GetSnackBar editSnacbar({
    required String message,
  }) {
    return GetSnackBar(
      margin: const EdgeInsets.all(16),
      snackPosition: SnackPosition.TOP,
      duration: const Duration(milliseconds: 1200),
      borderRadius: 35,
      barBlur: 1.0,
      backgroundColor: const Color(0xFF3DB6A0),
      icon: const Icon(
        Icons.check_circle_outline_outlined,
        size: 40,
        color: Colors.white,
      ),
      messageText: Text(
        message,
        style: const TextStyle(
          color: Colors.white,
          fontSize: 17,
          fontWeight: FontWeight.w400,
          fontFamily: 'sfpro',
        ),
      ),
    );
  }


}