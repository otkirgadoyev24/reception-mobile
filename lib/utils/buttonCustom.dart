import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SimpleAppButton extends StatelessWidget {
  final String text;
  final Color color;
  final Function OnTap;
  const SimpleAppButton({
    Key? key,
    required this.text,
    required this.color,
    required this.OnTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => OnTap(),
      child: Container(
        height: 48,
        width: Get.width,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(25),
        ),
        child: Center(
          child: Text(
            text.tr,
            style: const TextStyle(
              fontFamily: 'sfpro',
              fontWeight: FontWeight.w700,
              fontSize: 17,
              color: Colors.white,
              decoration: TextDecoration.none,
            ),
          ),
        ),
      ),
    );
  }
}