import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:statistic_app_v1/auth/login.dart';
import 'package:statistic_app_v1/pages/Finished.dart';
import 'package:statistic_app_v1/pages/InProgress.dart';
import 'package:statistic_app_v1/pages/NavbatdaTurgankar.dart';
import 'package:statistic_app_v1/pages/Reports.dart';
import 'package:statistic_app_v1/pages/Requests.dart';

import '../../theme/app_colors.dart';
import 'drawer_menu_widget.dart';

class DrawerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      height: Get.height,
      width: Get.width * .6,
      decoration: BoxDecoration(

        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 32,
          ),
          Row(
            children: [
              SizedBox(
                width: 24,
              ),
              Image.asset(
                'assets/images/ichki_ishlar.png',
                width: 87,
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          Text.rich(
            TextSpan(
              children: <InlineSpan>[
                TextSpan(
                  text: '"E-QABUL"',
                  style: GoogleFonts.poppins(
                    textStyle: const TextStyle(
                      color: Colors.blue,
                      fontSize: 26,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const Text(
            'ИЧКИ ИШЛАР ВАЗИРЛИГИ',
            style: TextStyle(
              color: lightGreyText,
              fontSize: 13,
              fontFamily: 'sfpro',
            ),
          ),
          const SizedBox(
            height: 32,
          ),
          DrawerMenuWidget(
              onTapEvent: () {},
              image: 'assets/images/home/Home.png',
              title: "Бош саҳифа".tr,
              subtitle: ''),
          DrawerMenuWidget(
              onTapEvent: () {
                Get.to(NewCreated());
              },
              image: 'assets/images/history_grey1.png',
              title: "Навбатда турганлар".tr,
              subtitle: ''),
          DrawerMenuWidget(
              onTapEvent: () {
                Get.to(Finished());
              },
              image: 'assets/images/done_job.png',
              title: "Қабулга кириб чиққанлар".tr,
              subtitle: ''),

          DrawerMenuWidget(
              onTapEvent: () {
                Get.offAll(Login());
              },
              image: 'assets/images/logout_icon.png',
              title: "Аккаунтдан чиқиш",
              subtitle: ''),

          const Spacer(),
        ],
      ),
    );
  }
}