

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../theme/CustomStyles.dart';
import '../../theme/app_colors.dart';

class DrawerMenuWidget extends StatelessWidget {
  final String image;
  final String title;
  final String subtitle;
  final Widget? trailing;
  final Function() onTapEvent;

  const DrawerMenuWidget(
      {Key? key,
        required this.image,
        required this.title,
        required this.subtitle,
        this.trailing,
        required this.onTapEvent})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppColor appcolor = AppColor();

    return InkWell(
      onTap: onTapEvent,
      child: SizedBox(
        height: 70,
        width: Get.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ImageIcon(
              AssetImage(image),
              size: 30,
              color: Color(0xff888686),
            ),
            const SizedBox(width: 16),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title.capitalizeFirst!,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'sfpro',
                    color: appcolor.black,
                  ),
                ),
                (subtitle != '')
                    ? Text(
                  subtitle,
                  style: CustomStyles.appBarSubtitleStyle,
                )
                    : const SizedBox(),
              ],
            ),

          ],
        ),
      ),
    );
  }
}