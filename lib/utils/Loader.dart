import 'package:flutter/material.dart';

import '../theme/app_colors.dart';


class Loader extends StatelessWidget {
final String bottom_title;
final bool isVisible;

const Loader({super.key, required this.bottom_title, required this.isVisible});
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: isVisible,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                width: 60,
                height: 60,
                decoration: const ShapeDecoration(
                  color: Colors.white,
                  shape: OvalBorder(),
                  shadows: [
                    BoxShadow(
                      color: Color(0x99E2E7F1),
                      blurRadius: 15,
                      offset: Offset(0, 4),
                      spreadRadius: 0,
                    )
                  ],
                ),
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: CircularProgressIndicator(
                    color: kprimaryColor,
                    strokeWidth: 2,
                  ),
                )),
            const SizedBox(
              height: 16,
            ),
            Text(
             bottom_title,
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: Color(0xFF273244),
                fontSize: 17,
                fontFamily: 'sfpro',
                fontWeight: FontWeight.w400,
                height: 1.65,
              ),
            )
          ],
        ),
      ),
    );
  }
}