import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:statistic_app_v1/controller/order_controller.dart';
import 'package:statistic_app_v1/theme/CustomStyles.dart';
import 'package:statistic_app_v1/theme/app_colors.dart';

class OrderCard extends StatelessWidget {
  final String lastName;
  final String firstName;
  final String middleName;
  final String avatar;
  final String category;
  final String place;
  final String date;

  OrderCard(
      {
        required this.avatar,
        required this.category,
        required this.place,
        required this.date, required this.lastName, required this.firstName, required this.middleName});

  final OrderController orderController = Get.put(OrderController());

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12),
      margin: const EdgeInsets.only(bottom: 8, top: 8),
      decoration: ShapeDecoration(
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, color: Color(0xFFCCD3E0)),
          borderRadius: BorderRadius.circular(12),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: 110,
                width: 110,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: CachedNetworkImage(
                    placeholder: (context, url) => Shimmer.fromColors(
                      baseColor: Colors.grey[300]!,
                      highlightColor: Colors.grey[100]!,
                      child: Container(
                        width: 110,
                        height: 110,
                        color: Colors.white,
                      ),
                    ),
                    fit: BoxFit.fill, imageUrl: avatar,
                  ),
                ),
              ),

              Container(
                height: 110,
                width: Get.width - 65 - 100 - 17-10-13+16,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          lastName.capitalizeFirst!,
                          style: TextStyle(overflow: TextOverflow.ellipsis,fontSize: 22),
                        ),
                        Text(
                          firstName.capitalizeFirst!,
                          style: TextStyle(overflow: TextOverflow.ellipsis,fontSize: 22),
                        ),
                        Text(
                          middleName.capitalizeFirst! ,
                          style: TextStyle(overflow: TextOverflow.ellipsis,fontSize: 22),
                        ),


                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 8,),
          Text(category,style: CustomStyles.appBarTitleStyle,),
          SizedBox(height: 8,),
          Text(place,style: CustomStyles.subHeaderTextStyle,),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
            Icon(Icons.access_time,color: borderColor,),
            SizedBox(width: 8,),
            Text(date,style: CustomStyles.subHeaderTextStyle)
          ],)
        ],
      ),
    );
  }
}