import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:statistic_app_v1/controller/login.dart';
import 'package:statistic_app_v1/home/HomeScreen.dart';
import 'package:statistic_app_v1/theme/CustomStyles.dart';
import 'package:statistic_app_v1/theme/app_colors.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

final LoginController loginController = Get.put(LoginController());

RxBool isShown = true.obs;
class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return loginController.getAccessType() == 'login'
        ? Obx(() => Scaffold(
            body: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(16),
              width: Get.width,
              height: Get.height,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/ichki_ishlar.png',
                      width: 75,
                      height: 75,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      '"E-QABUL"',
                      style: CustomStyles.appBarTitleStyle,
                    ),
                    Text(
                      'тизимига кириш',
                      style: CustomStyles.appBarTitleStyle,
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    SizedBox(
                      height: 50,
                      child: TextFormField(
                        controller: loginController.username,
                        decoration: InputDecoration(
                            labelText: 'login'.tr,
                            labelStyle: TextStyle(color: Colors.grey),
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: borderColor, width: 1))),
                      ),
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    SizedBox(
                      height: 50,
                      child: TextFormField(

                        obscuringCharacter: '*',
                        obscureText: isShown.value,
                        controller: loginController.password,
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            onPressed: (){
                           isShown.value = !isShown.value;
                            },
                            icon: isShown.value ? Icon(Icons.remove_red_eye_rounded): Icon(Icons.visibility_off),
                          ),
                            labelText: 'password'.tr,
                            labelStyle: TextStyle(color: Colors.grey),
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: borderColor, width: 1))),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            bottomNavigationBar: Container(
              padding: EdgeInsets.all(16),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(Get.width, 48),
                  backgroundColor: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25),
                  ),
                ),
                onPressed: () {
                  loginController.login();
                  //  Get.off(HomeScreen());
                },
                child: loginController.isLoading.value == true
                    ? Container(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                          strokeWidth: 2,
                        ),
                      )
                    : Text(
                        'loginIn'.tr,
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      ),
              ),
            )))
        : HomeScreen();
  }
}