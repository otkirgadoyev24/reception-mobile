// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:lottie/lottie.dart';
// import 'package:statistic_app_v1/controller/order_controller.dart';
// import 'package:statistic_app_v1/theme/app_colors.dart';
// import 'package:statistic_app_v1/utils/order_card.dart';
//
// import '../utils/drawer/drawer_screen.dart';
//
// class Requests extends StatelessWidget {
//
//
//
//   final OrderController controller = Get.put(OrderController());
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: appBgColor,
//
//       appBar: AppBar(
//         backgroundColor: Colors.blue,
//         title: Text('Сўровлар',style: TextStyle(color: Colors.white),),
//       ),
//       body:  Obx(() => Stack(
//         children: [
//           Container(
//             padding: EdgeInsets.symmetric(horizontal: 16),
//               width: Get.width,
//               height: Get.height,
//               child: controller.isLoading.value == true || controller.orders?.results == null
//                   ? Center(
//                 child: CircularProgressIndicator(
//                   color: kprimaryColor,
//                 ),
//               )
//                   : controller.requests.isEmpty ? Center(child: Text('Мурожаатлар мавжуд эмас'),) : ListView.builder(
//                 itemCount: controller.requests.length,
//                 itemBuilder: (BuildContext context, int index) {
//                   return OrderCard(
//                       murojaat_raqami: controller.requests![index]!.number!,
//                       id: controller.requests![index]!.id!.toString(),
//                       sana: controller.requests![index]!.createdAt!.toString().substring(0,10),
//                       fio: controller.requests![index]!.passportInform!,
//                       kelib_tushgan_joyi: controller.requests![index]!.toWhom!.workPlace!.name!,
//                       qabul_shakli: 'qabul_shakli',
//                       kimning_qabuliga: controller.requests![index]!.toWhom!.name!,
//                       mur_tas: '****',
//                       natijasi: controller.requests![index]!.result!,
//                       statusi: controller.requests![index]!.status!);
//                 },
//               )),
//           Center(child: Visibility(
//             visible: controller.isLoading2.value,
//               child: Lottie.asset('assets/animations/loader_pink.json',width: Get.width/3,height: Get.width/3)),)
//         ],
//       )),
//     );
//   }
// }