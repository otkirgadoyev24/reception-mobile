// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:lottie/lottie.dart';
// import 'package:statistic_app_v1/controller/order_controller.dart';
// import 'package:statistic_app_v1/theme/app_colors.dart';
// import 'package:statistic_app_v1/utils/order_card.dart';
//
// import '../utils/drawer/drawer_screen.dart';
//
// class InProgress extends StatelessWidget {
//
//
//
//   final OrderController controller = Get.put(OrderController());
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: appBgColor,
//
//       appBar: AppBar(
//         backgroundColor: Colors.blue,
//         title: Text('Жараёндаги мурожаатлар',style: TextStyle(color: Colors.white),),
//       ),
//       body:  Obx(() => Stack(
//         children: [
//           Container(
//             padding: EdgeInsets.symmetric(horizontal: 16),
//               width: Get.width,
//               height: Get.height,
//               child: controller.isLoading.value == true || controller.orders?.results == null
//                   ? Center(
//                 child: CircularProgressIndicator(
//                   color: kprimaryColor,
//                 ),
//               )
//                   : controller.new_created.isEmpty ? Center(child: Text('Мурожаатлар мавжуд эмас'),) : ListView.builder(
//                 itemCount: controller.new_created.length,
//                 itemBuilder: (BuildContext context, int index) {
//                   return OrderCard(
//                       murojaat_raqami: controller.in_progress![index]!.number!,
//                       id: controller.in_progress![index]!.id!.toString(),
//                       sana: controller.in_progress![index]!.createdAt!.toString().substring(0,10),
//                       fio: controller.in_progress![index]!.passportInform!,
//                       kelib_tushgan_joyi: controller.in_progress![index]!.toWhom!.workPlace!.name!,
//                       qabul_shakli: 'qabul_shakli',
//                       kimning_qabuliga: controller.in_progress![index]!.toWhom!.name!,
//                       mur_tas: '****',
//                       natijasi: controller.in_progress![index]!.result!,
//                       statusi: controller.in_progress![index]!.status!);
//                 },
//               )),
//           Center(child: Visibility(
//             visible: controller.isLoading2.value,
//               child: Lottie.asset('assets/animations/loader_pink.json',width: Get.width/3,height: Get.width/3)),)
//         ],
//       )),
//     );
//   }
// }