import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:statistic_app_v1/theme/app_colors.dart';

import '../controller/single_order_model.dart';

class FullInfo extends StatelessWidget {

  final String ? status;
   FullInfo({this.status});
  static SingleOrder? singleOrder;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appBgColor,
      appBar: AppBar(
        leading: IconButton(
          onPressed: Get.back,
          icon: Icon(
            Icons.chevron_left,
            color: Colors.white,
            size: 39,
          ),
        ),
        centerTitle: true,
        backgroundColor: kprimaryColor,
        title: Text(
          'Мурожаат   #${singleOrder!.number} ',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        width: Get.width,
        height: Get.height,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      width:160,
                      height: 180,
                      decoration: BoxDecoration(
                          border: Border.all(color: borderColor, width: 1)),
                      child:
                          singleOrder!.passportInform!.citizen!.avatar != null
                              ? CachedNetworkImage(
                            fit: BoxFit.fill,
                            imageUrl:   "${singleOrder!.passportInform!.citizen!.avatar!}",
                                  width:160,
                                  height: 180,
                                )
                              : SizedBox()),
                  SizedBox(
                    width: 32,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Item(
                            t2: "${singleOrder?.passportInform?.citizen?.lastName}".capitalizeFirst ??
                                "mavjud emas",
                            t1: 'Фамилияси'),
                        Item(
                            t2: "${singleOrder
                                ?.passportInform?.citizen?.firstName}".capitalizeFirst ??
                                "mavjud emas",
                            t1: 'Исми'),
                        Item(
                            t2:"${ singleOrder
                                ?.passportInform?.citizen?.middleName}".capitalizeFirst ??
                                "mavjud emas",
                            t1: 'Шарифи'),
                        Item(
                            t2: "${singleOrder?.passportInform?.nationality}".capitalizeFirst ??
                                "mavjud emas",
                            t1: 'Миллати'),
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Item(
                      t1: 'Туғилган санаси'.tr,
                      t2: singleOrder?.passportInform?.citizen?.birthDate
                              ?.toString()
                              .substring(0, 10) ??
                          "mavjud emas"),
                  Item(
                      t1: 'Пасспорт серия'.tr,
                      t2: singleOrder?.passportInform?.passportNumber ??
                          "mavjud emas"),

                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                children: [

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        constraints: BoxConstraints(
                            maxWidth: Get.width),
                        child: Text(
                          'Туғилган жойи',
                          style: TextStyle(
                              color: Color(0xFF8E97A7),
                              fontSize: 13,
                              fontFamily: 'sfpro',
                              fontWeight: FontWeight.w400,
                              overflow: TextOverflow.ellipsis
                          ),
                        ),
                      ),
                      Container(
                        constraints: BoxConstraints(maxWidth:  Get.width-32),
                        child: Text("${singleOrder?.passportInform?.birthPlace}".capitalizeFirst ??
                            "mavjud emas",
                          style: TextStyle(
                            color: black,
                            fontSize: 17,
                            fontFamily: 'sfpro',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ],
                  )

                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [

          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                constraints: BoxConstraints(
                    maxWidth: Get.width),
                child: Text(
                  'Рўйҳатда турган жойи',
                  style: TextStyle(
                      color: Color(0xFF8E97A7),
                      fontSize: 13,
                      fontFamily: 'sfpro',
                      fontWeight: FontWeight.w400,
                      overflow: TextOverflow.ellipsis
                  ),
                ),
              ),
              Container(
                constraints: BoxConstraints(maxWidth:  Get.width-32),
                child: Text("${singleOrder?.passportInform?.permamentAddress}".capitalizeFirst ??
                  "mavjud emas",
                  style: TextStyle(
                      color: black,
                      fontSize: 17,
                      fontFamily: 'sfpro',
                      fontWeight: FontWeight.w700,
                     ),
                ),
              ),
            ],
          )
                ],
              ),
              SizedBox(height: 16,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Item(
                      width: Get.width - 32,
                      t1: "Мурожаат рақами",
                      t2: singleOrder?.number ??
                          "mavjud emas"),
                ],
              ),
              SizedBox(height: 16,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Item(
                      width: Get.width - 32,
                      t1: "Келиб тушиш жойи",
                      t2: singleOrder?.applicationFrom?.name ?? "mavjud emas"




                  ),
                ],
              ) ,
              SizedBox(height: 16,),

              Row(children: [
                Item(
                    width: Get.width-32,
                    t1: "Мурожаат ўрганилган жой", t2: singleOrder?.toWhom?.workPlace?.name ?? "mavjud emas"
                )
              ],),
              SizedBox(height: 16,),
              // Row(children: [
              //
              //   Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Text(
              //         'Қабулига ёзилган рахбар'.capitalizeFirst!,
              //         style: TextStyle(
              //           color: Color(0xFF8E97A7),
              //           fontSize: 13,
              //           fontFamily: 'sfpro',
              //           fontWeight: FontWeight.w400,
              //         ),
              //       ),
              //       Container(
              //         constraints: BoxConstraints(maxWidth: Get.width-32),
              //         child: Text(
              //           "${singleOrder?.toWhom?.name ?? "mavjud emas"}",
              //           style: TextStyle(
              //             color: black,
              //             fontSize: 17,
              //             fontFamily: 'sfpro',
              //             fontWeight: FontWeight.w700,
              //           ),
              //         ),
              //       ),
              //     ],
              //   )
              //
              //
              //
              //
              //
              //
              //
              // ],),
              // SizedBox(height: 16,),
              Row(children: [

                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Мурожаат таснифи',
                      style: TextStyle(
                        color: Color(0xFF8E97A7),
                        fontSize: 13,
                        fontFamily: 'sfpro',
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: Get.width -32),
                      child: Text(
                        singleOrder?.category?.title ?? "mavjud emas",
                        style: TextStyle(
                          color: black,
                          fontSize: 17,
                          fontFamily: 'sfpro',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ],
                )




              ],) ,
              SizedBox(height: 16,),
              Row(children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Мурожаат матни',
                      style: TextStyle(
                        color: Color(0xFF8E97A7),
                        fontSize: 13,
                        fontFamily: 'sfpro',
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: Get.width -32),
                      child: Text(
                        singleOrder?.description ?? "mavjud emas",
                        style: TextStyle(
                            color: black,
                            fontSize: 17,
                            fontFamily: 'sfpro',
                            fontWeight: FontWeight.w700,
                           ),
                      ),
                    ),
                  ],
                )              ],),
              SizedBox(height: 16,),
              // Row(children: [
              //   Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Container(
              //         constraints: BoxConstraints(
              //           maxWidth: Get.width-32
              //         ),
              //         child: Text(
              //           'Мурожаатни қабул қилган шахс томонидан киритилган изоҳ',
              //           style: TextStyle(
              //             color: Color(0xFF8E97A7),
              //             fontSize: 13,
              //             fontFamily: 'sfpro',
              //             overflow: TextOverflow.ellipsis,
              //             fontWeight: FontWeight.w400,
              //           ),
              //         ),
              //       ),
              //       Container(
              //         constraints: BoxConstraints(maxWidth: Get.width -32),
              //         child: Text(
              //           singleOrder?.comment ?? "mavjud emas",
              //           style: TextStyle(
              //               color: black,
              //               fontSize: 17,
              //               fontFamily: 'sfpro',
              //               fontWeight: FontWeight.w700,
              //              ),
              //         ),
              //       ),
              //     ],
              //   )              ],),
              // SizedBox(height: 16,),
              // Row(children: [
              //   Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Text(
              //         'Мурожаатга илова қилинган файл',
              //         style: TextStyle(
              //           color: Color(0xFF8E97A7),
              //           fontSize: 13,
              //           fontFamily: 'sfpro',
              //           fontWeight: FontWeight.w400,
              //         ),
              //       ),
              //    // status != 'finished'  ? Container(
              //    //      constraints: BoxConstraints(maxWidth: Get.width -32),
              //    //      child: Text(
              //    //         "FILES",
              //    //        style: TextStyle(
              //    //            color: black,
              //    //            fontSize: 17,
              //    //            fontFamily: 'sfpro',
              //    //            fontWeight: FontWeight.w700,
              //    //           ),
              //    //      ),
              //    //    ):SizedBox(),
              //     ],
              //   )              ],),
              // SizedBox(height: 16,),
              // Row(children: [
              //   Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Text(
              //         'Мурожаат натижаси',
              //         style: TextStyle(
              //           color: Color(0xFF8E97A7),
              //           fontSize: 13,
              //           fontFamily: 'sfpro',
              //           fontWeight: FontWeight.w400,
              //         ),
              //       ),
              //       Container(
              //         constraints: BoxConstraints(maxWidth: Get.width -32),
              //         child: Text(
              //            singleOrder?.result ?? "mavjud emas",
              //           style: TextStyle(
              //               color: black,
              //               fontSize: 17,
              //               fontFamily: 'sfpro',
              //               fontWeight: FontWeight.w700,
              //              ),
              //         ),
              //       ),
              //     ],
              //   )              ],),
       // status != 'finished'?    Column(children: [   SizedBox(height: 16,),
       //       Row(children: [
       //         Column(
       //           crossAxisAlignment: CrossAxisAlignment.start,
       //           children: [
       //             Text(
       //               'Мурожаат натижаси бойича киритилган изоҳ',
       //               style: TextStyle(
       //                 color: Color(0xFF8E97A7),
       //                 fontSize: 13,
       //                 fontFamily: 'sfpro',
       //                 fontWeight: FontWeight.w400,
       //               ),
       //             ),
       //             Container(
       //               constraints: BoxConstraints(maxWidth: Get.width -32),
       //               child: Text(
       //                 singleOrder?.cancelReason ?? "mavjud emas",
       //                 style: TextStyle(
       //                   color: black,
       //                   fontSize: 17,
       //                   fontFamily: 'sfpro',
       //                   fontWeight: FontWeight.w700,
       //                 ),
       //               ),
       //             ),
       //           ],
       //         )              ],),],):SizedBox(),
              // SizedBox(height: 16,),
              // Row(children: [
              //   Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Text(
              //         'Берилган муддати',
              //         style: TextStyle(
              //           color: Color(0xFF8E97A7),
              //           fontSize: 13,
              //           fontFamily: 'sfpro',
              //           fontWeight: FontWeight.w400,
              //         ),
              //       ),
              //       Container(
              //         constraints: BoxConstraints(maxWidth: Get.width -32),
              //         child: Text(
              //            singleOrder!.expireDate!  < 0 ? "${singleOrder!.expireDate! * (-1)} кун ўтиб кетган" :singleOrder!.expireDate.toString(),
              //           style: TextStyle(
              //               color: black,
              //               fontSize: 17,
              //               fontFamily: 'sfpro',
              //               fontWeight: FontWeight.w700,
              //              ),
              //         ),
              //       ),
              //     ],
              //   )              ],),SizedBox(height: 64,)
            ],
          ),
        ),
      ),
    );
  }
}

class Item extends StatelessWidget {
  final String t1;
  final String t2;
  final double? width;

  Item({
    required this.t1,
    required this.t2,
    this.width,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          constraints: BoxConstraints(
              maxWidth: Get.width/2),
          child: Text(
            t1,
            style: TextStyle(
              color: Color(0xFF8E97A7),
              fontSize: 13,
              fontFamily: 'sfpro',
              fontWeight: FontWeight.w400,
                overflow: TextOverflow.ellipsis
            ),
          ),
        ),
        Container(
          constraints: BoxConstraints(maxWidth: width ?? Get.width / 2),
          child: Text(
            t2,
            style: TextStyle(
                color: black,
                fontSize: 17,
                fontFamily: 'sfpro',
                fontWeight: FontWeight.w700,
                overflow: TextOverflow.ellipsis),
          ),
        ),
      ],
    );
  }
}