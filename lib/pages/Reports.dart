// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:lottie/lottie.dart';
// import 'package:statistic_app_v1/controller/order_controller.dart';
// import 'package:statistic_app_v1/theme/app_colors.dart';
// import 'package:statistic_app_v1/utils/order_card.dart';
//
// import '../controller/finished_orders.dart';
// import '../utils/drawer/drawer_screen.dart';
//
// class Reports extends StatelessWidget {
//
//
//
//   final ReportController controller = Get.put(ReportController());
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: appBgColor,
//
//       appBar: AppBar(
//         backgroundColor: Colors.blue,
//         title: Text('Хисоботлар',style: TextStyle(color: Colors.white),),
//       ),
//       body:  Obx(() => Stack(
//         children: [
//           Container(
//             padding: EdgeInsets.symmetric(horizontal: 16),
//               width: Get.width,
//               height: Get.height,
//               child: controller.isLoading.value == true || controller.reports?.results == null
//                   ? Center(
//                 child: CircularProgressIndicator(
//                   color: kprimaryColor,
//                 ),
//               )
//                   : controller!.reports!.results!.isEmpty ? Center(child: Text('Мурожаатлар мавжуд эмас'),) :
//               ListView.builder(
//                 itemCount: controller?.reports?.results?.length,
//                 itemBuilder: (BuildContext context, int index) {
//                   return OrderCard(
//                       murojaat_raqami: controller?.reports?.results?[index]?.number ?? "***",
//                       id: controller?.reports?.results?[index]?.id.toString() ?? "***",
//                       sana: controller?.reports?.results?[index]!.createdAt!.toString().substring(0,10) ?? "**",
//                       fio: controller?.reports?.results?[index]?.passportInform ?? "",
//                       kelib_tushgan_joyi: controller?.reports?.results?[index]?.toWhom?.workPlace?.name ?? "",
//                       qabul_shakli: 'qabul_shakli',
//                       kimning_qabuliga: controller?.reports?.results?[index]?.toWhom?.name?? "",
//                       mur_tas: '****',
//                       natijasi: controller?.reports?.results?[index]?.result ?? "",
//                       statusi: controller?.reports?.results?[index]?.status ?? "");
//                 },
//               )),
//           Center(child: Visibility(
//             visible: controller.isLoading.value,
//               child: Lottie.asset('assets/animations/loader_pink.json',width: Get.width/3,height: Get.width/3)),)
//         ],
//       )),
//     );
//   }
// }