import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:statistic_app_v1/controller/order_controller.dart';
import 'package:statistic_app_v1/pages/FullIfoScreen.dart';
import 'package:statistic_app_v1/theme/app_colors.dart';
import 'package:statistic_app_v1/utils/Loader.dart';
import 'package:statistic_app_v1/utils/order_card.dart';

class NewCreated extends StatefulWidget {
  @override
  State<NewCreated> createState() => _NewCreatedState();
}

class _NewCreatedState extends State<NewCreated> {
  final OrderController controller = Get.put(OrderController());

  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  void initState() {
    controller.getOrderInProgress();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appBgColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.blue,
        title: Text(
          'Навбатда турганлар',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Obx(() => Stack(
        children: [
          Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              width: Get.width,
              height: Get.height,
              child: controller.isLoading.value == true
                  ? Center(
                      child: CircularProgressIndicator(
                        color: kprimaryColor,
                      ),
                    )
                  : (controller.ordersInProgress!.results!.isEmpty ||
                          controller.ordersInProgress == null ||
                          controller.ordersInProgress!.results == null)
                      ? Center(
                          child: Text('Мурожаатлар мавжуд эмас'),
                        )
                      : ListView.builder(
                          itemCount: controller.ordersInProgress!.results!.length,
                          itemBuilder: (BuildContext context, int index) {
                            var item = controller.ordersInProgress!.results![index];
                            return InkWell(
                              onTap: (){
                                controller.getSingleOrder(item.id.toString());

                              },
                              child: OrderCard(
                                 lastName:item.passportInform!.citizen!.lastName! ,
                                  firstName:item.passportInform!.citizen!.firstName! ,
                                  middleName: item.passportInform!.citizen!.middleName!,
                                  avatar: item.passportInform!.citizen!.avatar ?? "",
                                  category: item.category!.title ?? "",
                                  place: item.applicationFrom?.name ?? "",
                                  date: item.createdAt.toString().substring(0,10)),
                            );
                          },
                        )),
          Loader(bottom_title: "Юкланяпти", isVisible: controller.isLoading2.value)

        ],
      )),
    );
  }
}