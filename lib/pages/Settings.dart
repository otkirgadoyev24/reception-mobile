import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:statistic_app_v1/pages/change_language_screen.dart';
import 'package:statistic_app_v1/theme/app_colors.dart';

class Settings extends StatelessWidget {
  Settings({Key? key}) : super(key: key);
  final box = GetStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kprimaryColor,
        title: Text(
          'settings'.tr,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5), // Shadow color
                      spreadRadius: .5, // Spread radius
                      blurRadius: 7, // Blur radius
                      offset: Offset(0, 3), // Offset in x and y direction
                    ),
                  ],
                border: Border.all(color: lightGreyText,width: .5),
                  borderRadius: BorderRadius.circular(12)),
              child: ListTile(
                trailing: Text(
                  'lang_code'.tr,
                  style: TextStyle(color: Colors.black, fontSize: 17),
                ),
                onTap: () => Get.to(ChangeLanguageScreen()),
                title: Text(
                  "lang".tr,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                leading: Icon(
                  Icons.translate,
                  color: Colors.black,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}