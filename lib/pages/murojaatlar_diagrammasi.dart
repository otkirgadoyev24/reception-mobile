import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:statistic_app_v1/controller/order_controller.dart';
import 'package:statistic_app_v1/theme/CustomStyles.dart';
import 'package:statistic_app_v1/theme/app_colors.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../utils/drawer/drawer_screen.dart';

class Statistics extends StatelessWidget {
  Statistics({Key? key}) : super(key: key);

  final OrderController orderController = Get.put(OrderController());
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      drawerEnableOpenDragGesture: false,
      drawer: DrawerScreen(),
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            _key.currentState!.openDrawer();
          },
          icon: Image.asset(
            "assets/images/menu_bar.png",
            width: 30,
            height: 30,
            color: Colors.white,
          ),
        ),
        backgroundColor: kprimaryColor,
        title: Text(
          'Мурожаатлар ',
          style: TextStyle(color: white),
        ),
      ),
      body: Obx(() => Container(
            width: Get.width,
            height: Get.height,
            child: orderController.isLoading3.value || orderController
                .statistics['result_progress'] == null || orderController.categories[0] == null
                ||orderController.categories.isEmpty

                ? Center(
                    child: CircularProgressIndicator(
                      color: kprimaryColor,
                    ), //
                  )
                : SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.all(8),
                          child: Row(
                            children: [
                              Expanded(child:
                              Card(
                                child: Container(
                                  padding: EdgeInsets.all(8),

                                  child: Row(children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text('${orderController.navbatda}',style: TextStyle(
                                              color: Colors.black,fontSize: 33,fontWeight: FontWeight.bold
                                            ),),
                                             SizedBox(width: 32,),
                                             CircularPercentIndicator(
                                              radius: 30.0,
                                              lineWidth: 8.0,
                                              percent: orderController.navbatda/orderController.hammasi,
                                              backgroundColor: Color(0x964c91e8),

                                              center: new Text("${orderController.navbatda/orderController.hammasi*100}%".substring(0,2)),
                                              progressColor: Colors.blue,
                                            )


                                          ],
                                        ),
                                        SizedBox(height: 8,),
                                        Text('Навбатда турганлар',style: CustomStyles.appBarSubtitleStyle,),

                                      ],
                                    )
                                  ],),
                                ),
                              )),
                              SizedBox(width: 16,),
                              Expanded(child:
                              Card(
                                child: Container(
                                  padding: EdgeInsets.all(8),

                                  child: Row(children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text('${orderController.kirganlar}',style: TextStyle(
                                                color: Colors.black,fontSize: 33,fontWeight: FontWeight.bold
                                            ),),
                                            SizedBox(width: 32,),
                                            CircularPercentIndicator(
                                              radius: 30.0,
                                              lineWidth: 8.0,
                                              percent: orderController.kirganlar/orderController.hammasi,
                                              backgroundColor: Color(0x86f8a9d1),

                                              center: new Text("${orderController.kirganlar/orderController.hammasi*100}%".substring(0,2)),
                                              progressColor: Color(0xfff15ea7),
                                            )


                                          ],
                                        ),
                                        SizedBox(height: 8,),
                                        Text('Кириб чиққанлар',style: CustomStyles.appBarSubtitleStyle,),

                                      ],
                                    )
                                  ],),
                                ),
                              )),

                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(16),
                          child: SizedBox(
                            width: Get.width,
                            height:400,child: SfCartesianChart(
                            primaryXAxis: CategoryAxis(isVisible: false),
                            series: <ChartSeries>[
                              ColumnSeries<ChartData2, String>(
                                dataSource: [
                                  ChartData2('ИИО ларига ишга тиклаш', orderController.categories[0]['count'], Color(
                                      0xff47c25b)),
                                  ChartData2('Махкумларнинг жазо ўташи билан боғлиқ масалалар', orderController.categories[1]['count'], Color(
                                      0xffff0000)),
                                  ChartData2('ЙҲХ юзасидан масалалар', orderController.categories[2]['count'], Color(
                                      0xfffff43e)),
                                  ChartData2('Тергов-суриштирув масалалари', orderController.categories[3]['count'], Color(
                                      0xffc24747)),
                                  ChartData2('Меърос, мол-мулк масалалари', orderController.categories[4]['count'], Color(
                                      0xff840cab)),
                                ],
                                xValueMapper: (ChartData2 data, _) => data.category,

                                yValueMapper: (ChartData2 data, _) => data.value,
                                pointColorMapper: (ChartData2 data, _) => data.color,

      ),
                            ],
                          ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(16),
                          alignment: Alignment.topRight,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              Row(children: [
                                Container(
                                  width: 15,
                                  height: 15,
                                  color:  Color(
                                      0xff47c25b),
                                ),
                                Text(' ИИО ларига ишга тиклаш',style: TextStyle(),)
                              ],),
                              SizedBox(height: 8,),

                              Container(
                                constraints: BoxConstraints(
                                  maxWidth: Get.width-32
                                ),
                                child: FittedBox(
                                  child: Row(children: [
                                    Container(
                                      width: 15,
                                      height: 15,
                                      color:  Color(
                                          0xffff0000),
                                    ),
                                    Text(' Махкумларнинг жазо ўташи билан боғлиқ масалалар ')
                                  ],),
                                ),
                              ),
                              SizedBox(height: 8,),

                              Row(children: [
                                Container(
                                  width: 15,
                                  height: 15,
                                  color:  Color(
                                      0xfffff43e),
                                ),
                                Text(' ЙҲХ юзасидан масалалар ')
                              ],),
                              SizedBox(height: 8,),

                              Row(children: [
                                Container(
                                  width: 15,
                                  height: 15,
                                  color:  Color(
                                      0xffc24747),
                                ),
                                Text(' Тергов-суриштирув масалалари ')
                              ],),
                              SizedBox(height: 8,),

                              Row(children: [
                                Container(
                                  width: 15,
                                  height: 15,
                                  color:  Color(
                                      0xff840cab),
                                ),
                                Text(' Меърос, мол-мулк масалалари ')
                              ],),
                              SizedBox(height: 8,),

                            ],),
                        ),


                        Container(
                          child: SizedBox(
                            height: 300,
                            child: SfCircularChart(
                              legend: Legend(isVisible: true,overflowMode: LegendItemOverflowMode.wrap),
                              series: <CircularSeries>[
                                // Render pie chart
                                DoughnutSeries<ChartData, String>(
                                  explodeAll: true,
                                  dataSource: <ChartData>[
                                    ChartData(
                                        "Ўрганилмоқда",
                                        orderController
                                            .statistics['result_progress']
                                            .toDouble(),Color(0xff81B3F7)),
                                    ChartData(
                                        'Рад этилган',
                                        orderController.statistics['result_glad']
                                            .toDouble(),Color(0xffF883ED)),
                                    ChartData(
                                        'Тушунтириш берилган',
                                        orderController.statistics['result_explain']
                                            .toDouble(),Colors.blue), //+
                                    ChartData(
                                        "Кўрмасдан қолдирилган",
                                        orderController.statistics['result_not_see']
                                            .toDouble(),Color(0xffFD8190)), //+
                                    ChartData(
                                        "Тугатилган",
                                        orderController
                                            .statistics['result_finished']
                                            .toDouble(),Colors.yellow), //+
                                    ChartData(
                                        "Маълумот учун",
                                        orderController
                                            .statistics['result_for_info']
                                            .toDouble(),Color(0xff78FF9A)), //+
                                    ChartData(
                                        'Қаноатлантирилган',
                                        orderController
                                            .statistics['result_contentment']
                                            .toDouble(),Color(0xffFFCC81)), //+
                                  ],
                                  xValueMapper: (ChartData data, _) =>
                                      data.category,
                                  yValueMapper: (ChartData data, _) => data.value,
                                  dataLabelMapper: (ChartData data, _) => data.category,
                                  pointColorMapper: (ChartData data, _) => data.color,

                                ),
                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
          )),
    );
  }
}

class ChartData {
  ChartData(this.category, this.value,this.color);

  final String category;
  final double value;
  final Color color;
}
class ChartData2 {
  ChartData2(this.category, this.value,this.color);

  final String category;
  final int value;
  final Color color;

}