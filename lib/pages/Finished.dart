import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:statistic_app_v1/controller/finished_orders.dart';
import 'package:statistic_app_v1/controller/order_controller.dart';
import 'package:statistic_app_v1/theme/app_colors.dart';
import 'package:statistic_app_v1/utils/order_card.dart';

import '../utils/Loader.dart';
import '../utils/drawer/drawer_screen.dart';

class Finished extends StatelessWidget {



  final FinishedController controller = Get.put(FinishedController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appBgColor,

      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.blue,
        title: Text('Қабулга кириб чиққанлар'.capitalizeFirst!,style: TextStyle(color: Colors.white),),
      ),
      body: Obx(() => Stack(
        children: [
          Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              width: Get.width,
              height: Get.height,
              child: controller.isLoading.value == true
                  ? Center(
                child: CircularProgressIndicator(
                  color: kprimaryColor,
                ),
              )
                  : (
                  controller.finishedOrders == null ||
                  controller.finishedOrders!.results == null ||
                  controller.finishedOrders!.results!.isEmpty
              )
                  ? Center(
                child: Text('Мурожаатлар мавжуд эмас'),
              )
                  : ListView.builder(
                itemCount: controller.finishedOrders!.results!.length,
                itemBuilder: (BuildContext context, int index) {
                  var item = controller.finishedOrders!.results![index];
                  return InkWell(
                    onTap: (){
                      controller.getSingleOrder(item.id.toString());
                    },
                    child: OrderCard(
                        lastName:item.passportInform!.citizen!.lastName! ,
                        firstName:item.passportInform!.citizen!.firstName! ,
                        middleName: item.passportInform!.citizen!.middleName!,
                        avatar: item.passportInform!.citizen!.avatar ?? "",
                        category: item.category!.title ?? "",
                        place: item.result ?? "",
                        date: item.createdAt.toString().substring(0,10)),
                  );
                },
              )),
          Loader(bottom_title: "Юкланяпти", isVisible: controller.isLoading2.value)

        ],
      )),
    );
  }
}