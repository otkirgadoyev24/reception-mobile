// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:lottie/lottie.dart';
// import 'package:statistic_app_v1/controller/order_controller.dart';
// import 'package:statistic_app_v1/theme/app_colors.dart';
// import 'package:statistic_app_v1/utils/order_card.dart';
//
// import '../utils/drawer/drawer_screen.dart';
//
// class MainScreen extends StatelessWidget {
//
//
//
//   final OrderController controller = Get.put(OrderController());
//   final GlobalKey<ScaffoldState> _key = GlobalKey();
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: appBgColor,
//       key: _key,
//       drawerEnableOpenDragGesture: false,
//       drawer: DrawerScreen(),
//       appBar: AppBar(
//
//         leading: IconButton(
//           onPressed: (){
//             _key.currentState!.openDrawer();
//
//           },
//           icon:Image.asset(
//           "assets/images/menu_bar.png",
//           width: 30,
//           height: 30,
//           color: white,
//         ),
//         ),
//         backgroundColor: kprimaryColor,
//         title: Text('Мурожаатлар ',style: TextStyle(color: Colors.white),),
//       ),
//       body:  Obx(() => Stack(
//         children: [
//           Container(
//             padding: EdgeInsets.symmetric(horizontal: 16),
//               width: Get.width,
//               height: Get.height,
//               child: controller.isLoading.value == true || controller.orders?.results == null
//                   ? Center(
//                 child: CircularProgressIndicator(
//                   color: kprimaryColor,
//                 ),
//               )
//                   : ListView.builder(
//                 itemCount: controller.orders?.results?.map((e) => e.status == status).toList().length,
//                 itemBuilder: (BuildContext context, int index) {
//                   return OrderCard(
//                       murojaat_raqami: controller.orders!.results![index]!.number!,
//                       id: controller.orders!.results![index]!.id!.toString(),
//                       sana: controller.orders!.results![index]!.createdAt!.toString().substring(0,10),
//                       fio: controller.orders!.results![index]!.passportInform!,
//                       kelib_tushgan_joyi: controller.orders!.results![index]!.toWhom!.workPlace!.name!,
//                       qabul_shakli: 'qabul_shakli',
//                       kimning_qabuliga: controller.orders!.results![index]!.toWhom!.name!,
//                       mur_tas: '****',
//                       natijasi: controller.orders!.results![index]!.result!,
//                       statusi: controller.orders!.results![index]!.status!);
//                 },
//               )),
//           Center(child: Visibility(
//             visible: controller.isLoading2.value,
//               child: Lottie.asset('assets/animations/loader_pink.json',width: Get.width/3,height: Get.width/3)),)
//         ],
//       )),
//     );
//   }
// }