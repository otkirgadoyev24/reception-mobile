
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:statistic_app_v1/controller/single_order_model.dart';

import '../exeptions/success_message.dart';
import '../pages/FullIfoScreen.dart';
import 'order_model_in_progress.dart';

class OrderController extends GetxController {
  RxBool isLoading = false.obs;
  RxBool isLoading2 = false.obs;
  RxBool isLoading3 = false.obs;
  RxMap statistics = {}.obs;
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  var box = GetStorage();
  var dio = Dio();
  List categories = [];

  InProgressOrder? ordersInProgress;
  int navbatda = 0;
  int kirganlar = 0;
  int hammasi = 0;

  @override
  onInit() {
    getOrderInProgress();
    getFullStatistics();
    getDashboardStatistics();
    getCategories();
    super.onInit();
  }

  getOrderInProgress() async {
    isLoading.value = true;
    try {
      var response = await dio.get(
        "https://0b7b-213-230-99-159.ngrok-free.app/api/v1/mobile/applications/list/",
        queryParameters: {'limit': 1000, 'offset': 0, 'status': 'in_progress'},
        options:
            Options(headers: {'Authorization': 'Bearer ${box.read('token')}'}),
      );
      if (response.statusCode == 200) {
        print('orders  ${response.data} ');
        isLoading.value = true;
        ordersInProgress = InProgressOrder.fromJson(response.data);
      }
    } on DioException catch (e) {
      Get.showSnackbar(
        SuccessMessageSnack().cancelSnackBar(message: e.response.toString()),
      );
    }
    isLoading.value = false;
  }

//   getReports() async {
//     isLoading.value = true;
//     try {
//       var response = await dio.get(
//           "http://b90e-213-230-99-159.ngrok-free.app/api/v1/mobile/applications/list",
//           queryParameters: {
//             'limit':1000,
//              'offset':0
//           },
//           options: Options(headers: {'Authorization': 'Bearer ${box.read('token')}'}),
// );
//       if (response.statusCode == 200) {
//         print('orders  ${response.data} ');
//         isLoading.value = true;
//         orders = Order.fromJson(response.data);
//         new_created = orders?.results?.where((e) => e.status == 'new_created').toList() ?? [];
//         in_progress = orders?.results?.where((e) => e.status == 'in_progress').toList() ?? [];
//         finished = orders?.results?.where((e) => e.status ==     'finished').toList() ?? [];
//         requests = orders?.results?.where((e) => e.status ==     'in_request').toList() ?? [];
//
//       }
//     } on DioException catch (e) {
//       Get.showSnackbar(
//         SuccessMessageSnack().cancelSnackBar(
//             message:
//             e.response.toString()
//         ),
//       );
//     }
//     isLoading.value = false;
//   }
  getSingleOrder(String id) async {
    isLoading2.value = true;
    try {
      var response = await dio.get(
          "https://0b7b-213-230-99-159.ngrok-free.app/api/v1/mobile/applications/$id/",
          options: Options(headers: {'Authorization': 'Bearer ${box.read('token')}'}),
);
      if (response.statusCode == 200) {
        isLoading2.value = true;
        FullInfo.singleOrder = SingleOrder.fromJson(response.data);
        Get.to(FullInfo());

      }
    } on DioException catch (e) {
      // Get.showSnackbar(
      //   SuccessMessageSnack().cancelSnackBar(
      //       message:
      //       e.response.toString()
      //   ),
      // );
    }
    isLoading2.value = false;
  }
//   getMonthlyData() async {
//     isLoading3.value = true;
//     try {
//       var response = await dio.get(
//           "https://b90e-213-230-99-159.ngrok-free.app/api/v1/common/statistics/monthly/",
//           options: Options(headers: {'Authorization': 'Bearer ${box.read('token')}'}),
// );
//       if (response.statusCode == 200) {
//         isLoading3.value = true;
//         monthlyData = response.data;
//
//
//       }
//     } on DioException catch (e) {
//       // Get.showSnackbar(
//       //   SuccessMessageSnack().cancelSnackBar(
//       //       message:
//       //       e.response.toString()
//       //   ),
//       // );
//     }
//     isLoading3.value = false;
//   }
  getFullStatistics() async {
    isLoading3.value = true;
    try {
      var response = await dio.get(
          "https://0b7b-213-230-99-159.ngrok-free.app/api/v1/common/statistics/",
          options: Options(headers: {'Authorization': 'Bearer ${box.read('token')}'}),
);
      if (response.statusCode == 200) {
        isLoading3.value = true;
        statistics.value = response.data;


      }
    } on DioException catch (e) {
      // Get.showSnackbar(
      //   SuccessMessageSnack().cancelSnackBar(
      //       message:
      //       e.response.toString()
      //   ),
      // );
    }
    isLoading3.value = false;
  }
  getDashboardStatistics() async {
    isLoading3.value = true;
    try {
      var response = await dio.get(
          "https://0b7b-213-230-99-159.ngrok-free.app/api/v1/common/statistics/dashboard/",
          options: Options(headers: {'Authorization': 'Bearer ${box.read('token')}'}),
);
      if (response.statusCode == 200) {
        isLoading3.value = true;
        navbatda = response.data['in_queue'];
        kirganlar = response.data['entrants'];
        hammasi = response.data['all_appeals'];


      }
    } on DioException catch (e) {
      // Get.showSnackbar(
      //   SuccessMessageSnack().cancelSnackBar(
      //       message:
      //       e.response.toString()
      //   ),
      // );
    }
    isLoading3.value = false;
  }
  getCategories() async {
    isLoading3.value = true;
    try {
      var response = await dio.get(
          "https://0b7b-213-230-99-159.ngrok-free.app/api/v1/common/statistics/category/",
          options: Options(headers: {'Authorization': 'Bearer ${box.read('token')}'}),
);
      if (response.statusCode == 200) {
        isLoading3.value = true;
        categories = response.data['results'];



      }
    } on DioException catch (e) {
      // Get.showSnackbar(
      //   SuccessMessageSnack().cancelSnackBar(
      //       message:
      //       e.response.toString()
      //   ),
      // );
    }
    isLoading3.value = false;
  }

  bool isTokenExpired() {
    // Get the current date and time
    DateTime now = DateTime.now();
    if (box.read('token_created_time') == null) {
      return true;
    }
    return now.isAfter(DateTime.parse(box.read('token_created_time')));
  }

  String getAccessType() {
    print('takin---------> ${box.read('token')}');

    if (box.read('token') == null || isTokenExpired()) {
      return "login";
    } else {
      return "home";
    }
  }
}