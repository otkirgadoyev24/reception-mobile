import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:statistic_app_v1/home/HomeScreen.dart';

import '../exeptions/success_message.dart';

class LoginController extends GetxController {
  RxBool isLoading = false.obs;
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  var box = GetStorage();
  var dio = Dio();

  login() async {
    isLoading.value = true;
    try {
      var response = await dio.post(
        "https://0b7b-213-230-99-159.ngrok-free.app/api/v1/staff/login/",
          data: {"username": username.text, "password": password.text});
      if (response.statusCode == 200) {

        isLoading.value = true;
        box.write('token', response.data['access'].toString());
        print('ACCES TOKEN ${box.read('token')}');
        box.write('token_created_time', DateTime.now().toString());
        Get.off(HomeScreen());
      }
    } on DioException catch (e) {

      if( e.response!.data['detail'].contains("No active account found")) {
        Get.showSnackbar(
          SuccessMessageSnack().cancelSnackBar(
              message:"Foydalanuvchi topilmadi"
          ),
        );

      }
      else {
        Get.showSnackbar(
          SuccessMessageSnack().cancelSnackBar(
              message:"Server error"
          ),
        );
      }

    }
    isLoading.value = false;
  }

  bool isTokenExpired() {
    // Get the current date and time
    DateTime now = DateTime.now();
     if (box.read('token_created_time') == null) {
       return true;
     }
    return now.isAfter(DateTime.parse(box.read('token_created_time')));
  }

  String getAccessType(){
    print('TOOKKKEENN  ${GetStorage().read('token')}');
    if(GetStorage().read('token') == null || isTokenExpired()) {
      return "login";
    }
    else {
      return "home";
    }
  }


}