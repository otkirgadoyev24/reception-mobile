// To parse this JSON data, do
//
//     final reports = reportsFromJson(jsonString);

import 'dart:convert';

ReportModel reportsFromJson(String str) => ReportModel.fromJson(json.decode(str));

String reportsToJson(ReportModel data) => json.encode(data.toJson());

class ReportModel {
  int? count;
  dynamic next;
  dynamic previous;
  List<Result>? results;

  ReportModel({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  factory ReportModel.fromJson(Map<String, dynamic> json) => ReportModel(
    count: json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? [] : List<Result>.from(json["results"]!.map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "next": next,
    "previous": previous,
    "results": results == null ? [] : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Result {
  int? id;
  String? number;
  DateTime? createdAt;
  String? passportInform;
  String? acceptionForm;
  ApplicationFrom? toWhom;
  String? description;
  String? result;
  String? status;
  Category? category;
  ApplicationFrom? applicationFrom;
  String? comment;
  int? expireDate;
  dynamic answer;
  dynamic requestType;
  ApplicationFrom? forwardTo;
  dynamic cancelReason;
  dynamic finishedAt;
  bool? isVerified;
  Staff? staff;

  Result({
    this.id,
    this.number,
    this.createdAt,
    this.passportInform,
    this.acceptionForm,
    this.toWhom,
    this.description,
    this.result,
    this.status,
    this.category,
    this.applicationFrom,
    this.comment,
    this.expireDate,
    this.answer,
    this.requestType,
    this.forwardTo,
    this.cancelReason,
    this.finishedAt,
    this.isVerified,
    this.staff,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"],
    number: json["number"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    passportInform: json["passport_inform"],
    acceptionForm: json["acception_form"],
    toWhom: json["to_whom"] == null ? null : ApplicationFrom.fromJson(json["to_whom"]),
    description: json["description"],
    result: json["result"],
    status: json["status"],
    category: json["category"] == null ? null : Category.fromJson(json["category"]),
    applicationFrom: json["application_from"] == null ? null : ApplicationFrom.fromJson(json["application_from"]),
    comment: json["comment"],
    expireDate: json["expire_date"],
    answer: json["answer"],
    requestType: json["request_type"],
    forwardTo: json["forward_to"] == null ? null : ApplicationFrom.fromJson(json["forward_to"]),
    cancelReason: json["cancel_reason"],
    finishedAt: json["finished_at"],
    isVerified: json["is_verified"],
    staff: json["staff"] == null ? null : Staff.fromJson(json["staff"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "number": number,
    "created_at": "${createdAt!.year.toString().padLeft(4, '0')}-${createdAt!.month.toString().padLeft(2, '0')}-${createdAt!.day.toString().padLeft(2, '0')}",
    "passport_inform": passportInform,
    "acception_form": acceptionForm,
    "to_whom": toWhom?.toJson(),
    "description": description,
    "result": result,
    "status": status,
    "category": category?.toJson(),
    "application_from": applicationFrom?.toJson(),
    "comment": comment,
    "expire_date": expireDate,
    "answer": answer,
    "request_type": requestType,
    "forward_to": forwardTo?.toJson(),
    "cancel_reason": cancelReason,
    "finished_at": finishedAt,
    "is_verified": isVerified,
    "staff": staff?.toJson(),
  };
}

class ApplicationFrom {
  int? id;
  String? name;
  int? parent;
  bool? active;
  ApplicationFrom? workPlace;

  ApplicationFrom({
    this.id,
    this.name,
    this.parent,
    this.active,
    this.workPlace,
  });

  factory ApplicationFrom.fromJson(Map<String, dynamic> json) => ApplicationFrom(
    id: json["id"],
    name: json["name"],
    parent: json["parent"],
    active: json["active"],
    workPlace: json["work_place"] == null ? null : ApplicationFrom.fromJson(json["work_place"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "parent": parent,
    "active": active,
    "work_place": workPlace?.toJson(),
  };
}

class Category {
  int? id;
  String? title;
  bool? active;

  Category({
    this.id,
    this.title,
    this.active,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    title: json["title"],
    active: json["active"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "active": active,
  };
}

class Staff {
  int? id;
  String? username;
  String? firstName;
  String? middleName;
  String? lastName;
  String? phoneNumber;
  String? rank;
  String? position;
  ApplicationFrom? workPlace;
  String? role;
  String? avatar;

  Staff({
    this.id,
    this.username,
    this.firstName,
    this.middleName,
    this.lastName,
    this.phoneNumber,
    this.rank,
    this.position,
    this.workPlace,
    this.role,
    this.avatar,
  });

  factory Staff.fromJson(Map<String, dynamic> json) => Staff(
    id: json["id"],
    username: json["username"],
    firstName: json["first_name"],
    middleName: json["middle_name"],
    lastName: json["last_name"],
    phoneNumber: json["phone_number"],
    rank: json["rank"],
    position: json["position"],
    workPlace: json["work_place"] == null ? null : ApplicationFrom.fromJson(json["work_place"]),
    role: json["role"],
    avatar: json["avatar"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "username": username,
    "first_name": firstName,
    "middle_name": middleName,
    "last_name": lastName,
    "phone_number": phoneNumber,
    "rank": rank,
    "position": position,
    "work_place": workPlace?.toJson(),
    "role": role,
    "avatar": avatar,
  };
}