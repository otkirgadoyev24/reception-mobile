// To parse this JSON data, do
//
//     final singleOrder = singleOrderFromJson(jsonString);

import 'dart:convert';

SingleOrder singleOrderFromJson(String str) => SingleOrder.fromJson(json.decode(str));

String singleOrderToJson(SingleOrder data) => json.encode(data.toJson());

class SingleOrder {
  int? id;
  String? number;
  DateTime? createdAt;
  PassportInform? passportInform;
  String? acceptionForm;
  ApplicationFrom? toWhom;
  String? description;
  dynamic result;
  String? status;
  Category? category;
  ApplicationFrom? applicationFrom;
  String? comment;
  int? expireDate;
  String? answer;
  dynamic requestType;
  ApplicationFrom? forwardTo;
  dynamic cancelReason;
  dynamic finishedAt;
  bool? isVerified;
  Staff? staff;
  Files? files;

  SingleOrder({
    this.id,
    this.number,
    this.createdAt,
    this.passportInform,
    this.acceptionForm,
    this.toWhom,
    this.description,
    this.result,
    this.status,
    this.category,
    this.applicationFrom,
    this.comment,
    this.expireDate,
    this.answer,
    this.requestType,
    this.forwardTo,
    this.cancelReason,
    this.finishedAt,
    this.isVerified,
    this.staff,
    this.files,
  });

  factory SingleOrder.fromJson(Map<String, dynamic> json) => SingleOrder(
    id: json["id"],
    number: json["number"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    passportInform: json["passport_inform"] == null ? null : PassportInform.fromJson(json["passport_inform"]),
    acceptionForm: json["acception_form"],
    toWhom: json["to_whom"] == null ? null : ApplicationFrom.fromJson(json["to_whom"]),
    description: json["description"],
    result: json["result"],
    status: json["status"],
    category: json["category"] == null ? null : Category.fromJson(json["category"]),
    applicationFrom: json["application_from"] == null ? null : ApplicationFrom.fromJson(json["application_from"]),
    comment: json["comment"],
    expireDate: json["expire_date"],
    answer: json["answer"],
    requestType: json["request_type"],
    forwardTo: json["forward_to"] == null ? null : ApplicationFrom.fromJson(json["forward_to"]),
    cancelReason: json["cancel_reason"],
    finishedAt: json["finished_at"],
    isVerified: json["is_verified"],
    staff: json["staff"] == null ? null : Staff.fromJson(json["staff"]),
    files: json["files"] == null ? null : Files.fromJson(json["files"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "number": number,
    "created_at": createdAt?.toIso8601String(),
    "passport_inform": passportInform?.toJson(),
    "acception_form": acceptionForm,
    "to_whom": toWhom?.toJson(),
    "description": description,
    "result": result,
    "status": status,
    "category": category?.toJson(),
    "application_from": applicationFrom?.toJson(),
    "comment": comment,
    "expire_date": expireDate,
    "answer": answer,
    "request_type": requestType,
    "forward_to": forwardTo?.toJson(),
    "cancel_reason": cancelReason,
    "finished_at": finishedAt,
    "is_verified": isVerified,
    "staff": staff?.toJson(),
    "files": files?.toJson(),
  };
}

class ApplicationFrom {
  int? id;
  String? name;
  int? parent;
  bool? active;
  ApplicationFrom? workPlace;

  ApplicationFrom({
    this.id,
    this.name,
    this.parent,
    this.active,
    this.workPlace,
  });

  factory ApplicationFrom.fromJson(Map<String, dynamic> json) => ApplicationFrom(
    id: json["id"],
    name: json["name"],
    parent: json["parent"],
    active: json["active"],
    workPlace: json["work_place"] == null ? null : ApplicationFrom.fromJson(json["work_place"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "parent": parent,
    "active": active,
    "work_place": workPlace?.toJson(),
  };
}

class Category {
  int? id;
  String? title;
  bool? active;

  Category({
    this.id,
    this.title,
    this.active,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    title: json["title"],
    active: json["active"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "active": active,
  };
}

class Files {
  List<dynamic>? data;

  Files({
    this.data,
  });

  factory Files.fromJson(Map<String, dynamic> json) => Files(
    data: json["data"] == null ? [] : List<dynamic>.from(json["data"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x)),
  };
}

class PassportInform {
  int? id;
  Citizen? citizen;
  String? passportNumber;
  String? pnfl;
  String? nationality;
  String? birthPlace;
  String? permamentAddress;

  PassportInform({
    this.id,
    this.citizen,
    this.passportNumber,
    this.pnfl,
    this.nationality,
    this.birthPlace,
    this.permamentAddress,
  });

  factory PassportInform.fromJson(Map<String, dynamic> json) => PassportInform(
    id: json["id"],
    citizen: json["citizen"] == null ? null : Citizen.fromJson(json["citizen"]),
    passportNumber: json["passport_number"],
    pnfl: json["pnfl"],
    nationality: json["nationality"],
    birthPlace: json["birth_place"],
    permamentAddress: json["permament_address"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "citizen": citizen?.toJson(),
    "passport_number": passportNumber,
    "pnfl": pnfl,
    "nationality": nationality,
    "birth_place": birthPlace,
    "permament_address": permamentAddress,
  };
}

class Citizen {
  int? id;
  String? lastName;
  String? firstName;
  String? middleName;
  dynamic gender;
  DateTime? birthDate;
  String? phoneNumber;
  String? avatar;

  Citizen({
    this.id,
    this.lastName,
    this.firstName,
    this.middleName,
    this.gender,
    this.birthDate,
    this.phoneNumber,
    this.avatar,
  });

  factory Citizen.fromJson(Map<String, dynamic> json) => Citizen(
    id: json["id"],
    lastName: json["last_name"],
    firstName: json["first_name"],
    middleName: json["middle_name"],
    gender: json["gender"],
    birthDate: json["birth_date"] == null ? null : DateTime.parse(json["birth_date"]),
    phoneNumber: json["phone_number"],
    avatar: json["avatar"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "last_name": lastName,
    "first_name": firstName,
    "middle_name": middleName,
    "gender": gender,
    "birth_date": "${birthDate!.year.toString().padLeft(4, '0')}-${birthDate!.month.toString().padLeft(2, '0')}-${birthDate!.day.toString().padLeft(2, '0')}",
    "phone_number": phoneNumber,
    "avatar": avatar,
  };
}

class Staff {
  int? id;
  String? username;
  String? firstName;
  String? middleName;
  String? lastName;
  String? phoneNumber;
  String? rank;
  String? position;
  ApplicationFrom? workPlace;
  String? role;
  dynamic avatar;

  Staff({
    this.id,
    this.username,
    this.firstName,
    this.middleName,
    this.lastName,
    this.phoneNumber,
    this.rank,
    this.position,
    this.workPlace,
    this.role,
    this.avatar,
  });

  factory Staff.fromJson(Map<String, dynamic> json) => Staff(
    id: json["id"],
    username: json["username"],
    firstName: json["first_name"],
    middleName: json["middle_name"],
    lastName: json["last_name"],
    phoneNumber: json["phone_number"],
    rank: json["rank"],
    position: json["position"],
    workPlace: json["work_place"] == null ? null : ApplicationFrom.fromJson(json["work_place"]),
    role: json["role"],
    avatar: json["avatar"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "username": username,
    "first_name": firstName,
    "middle_name": middleName,
    "last_name": lastName,
    "phone_number": phoneNumber,
    "rank": rank,
    "position": position,
    "work_place": workPlace?.toJson(),
    "role": role,
    "avatar": avatar,
  };
}