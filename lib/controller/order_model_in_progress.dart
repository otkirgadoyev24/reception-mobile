// To parse this JSON data, do
//
//     final inProgressOrder = inProgressOrderFromJson(jsonString);

import 'dart:convert';

InProgressOrder inProgressOrderFromJson(String str) => InProgressOrder.fromJson(json.decode(str));

String inProgressOrderToJson(InProgressOrder data) => json.encode(data.toJson());

class InProgressOrder {
  int? count;
  dynamic next;
  dynamic previous;
  List<Result>? results;

  InProgressOrder({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  factory InProgressOrder.fromJson(Map<String, dynamic> json) => InProgressOrder(
    count: json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? [] : List<Result>.from(json["results"]!.map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "next": next,
    "previous": previous,
    "results": results == null ? [] : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Result {
  int? id;
  PassportInform? passportInform;
  ApplicationFrom? applicationFrom;
  Category? category;
  String? result;
  DateTime? createdAt;

  Result({
    this.id,
    this.passportInform,
    this.applicationFrom,
    this.category,
    this.result,
    this.createdAt,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"],
    passportInform: json["passport_inform"] == null ? null : PassportInform.fromJson(json["passport_inform"]),
    applicationFrom: json["application_from"] == null ? null : ApplicationFrom.fromJson(json["application_from"]),
    category: json["category"] == null ? null : Category.fromJson(json["category"]),
    result: json["result"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "passport_inform": passportInform?.toJson(),
    "application_from": applicationFrom?.toJson(),
    "category": category?.toJson(),
    "result": result,
    "created_at": "${createdAt!.year.toString().padLeft(4, '0')}-${createdAt!.month.toString().padLeft(2, '0')}-${createdAt!.day.toString().padLeft(2, '0')}",
  };
}

class ApplicationFrom {
  int? id;
  String? name;
  int? parent;
  bool? active;

  ApplicationFrom({
    this.id,
    this.name,
    this.parent,
    this.active,
  });

  factory ApplicationFrom.fromJson(Map<String, dynamic> json) => ApplicationFrom(
    id: json["id"],
    name: json["name"],
    parent: json["parent"],
    active: json["active"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "parent": parent,
    "active": active,
  };
}

class Category {
  int? id;
  String? title;
  bool? active;

  Category({
    this.id,
    this.title,
    this.active,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    title: json["title"],
    active: json["active"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "active": active,
  };
}

class PassportInform {
  int? id;
  Citizen? citizen;

  PassportInform({
    this.id,
    this.citizen,
  });

  factory PassportInform.fromJson(Map<String, dynamic> json) => PassportInform(
    id: json["id"],
    citizen: json["citizen"] == null ? null : Citizen.fromJson(json["citizen"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "citizen": citizen?.toJson(),
  };
}

class Citizen {
  int? id;
  String? firstName;
  String? lastName;
  String? middleName;
  String? avatar;

  Citizen({
    this.id,
    this.firstName,
    this.lastName,
    this.middleName,
    this.avatar,
  });

  factory Citizen.fromJson(Map<String, dynamic> json) => Citizen(
    id: json["id"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    middleName: json["middle_name"],
    avatar: json["avatar"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "first_name": firstName,
    "last_name": lastName,
    "middle_name": middleName,
    "avatar": avatar,
  };
}