import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SettingControllerCustom extends GetxController {
  RxBool isNotificationOn = true.obs;
  RxBool isDarkMode = false.obs;
  final box = GetStorage();

  saveThemeStatus() async {
    box.write('theme', isDarkMode.value);
  }

  getThemeStatus() async {
    isDarkMode.value = (box.read('theme') ?? false);
  }
}