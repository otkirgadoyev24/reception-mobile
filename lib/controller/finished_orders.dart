import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:statistic_app_v1/controller/ReportModel.dart';
import 'package:statistic_app_v1/controller/order_model_in_finished.dart';
import 'package:statistic_app_v1/controller/single_order_model.dart';
import 'package:statistic_app_v1/home/HomeScreen.dart';
import 'package:statistic_app_v1/pages/FullIfoScreen.dart';

import '../exeptions/success_message.dart';
import '../pages/Reports.dart';
import 'order_model_in_progress.dart';

class FinishedController extends GetxController {
  RxBool isLoading = false.obs;
  RxBool isLoading2 = false.obs;

  var box = GetStorage();
  var dio = Dio();
  finishedOrder ? finishedOrders;


  @override
  onInit(){
    getFinishedOrders();
    super.onInit();


  }

  getFinishedOrders() async {
    isLoading.value = true;
    try {
      var response = await dio.get(
        "https://0b7b-213-230-99-159.ngrok-free.app/api/v1/mobile/applications/list/",
        queryParameters: {'limit': 1000, 'offset': 0, 'status': 'finished'},
        options:
        Options(headers: {'Authorization': 'Bearer ${box.read('token')}'}),
      );
      if (response.statusCode == 200) {
        isLoading.value = true;
        finishedOrders = finishedOrder.fromJson(response.data);
      }
    } on DioException catch (e) {
      Get.showSnackbar(
        SuccessMessageSnack().cancelSnackBar(message: e.response.toString()),
      );
    }
    isLoading.value = false;
  }

  getSingleOrder(String id) async {
    isLoading2.value = true;
    try {
      var response = await dio.get(
        "https://0b7b-213-230-99-159.ngrok-free.app/api/v1/mobile/applications/$id/",
        options: Options(headers: {'Authorization': 'Bearer ${box.read('token')}'}),
      );
      if (response.statusCode == 200) {
        isLoading2.value = true;
        FullInfo.singleOrder = SingleOrder.fromJson(response.data);
        Get.to(FullInfo(status: 'finished',));

      }
    } on DioException catch (e) {
      // Get.showSnackbar(
      //   SuccessMessageSnack().cancelSnackBar(
      //       message:
      //       e.response.toString()
      //   ),
      // );
    }
    isLoading2.value = false;
  }





}